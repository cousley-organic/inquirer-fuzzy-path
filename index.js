const fs = require('fs');
const path = require('path');
const util = require('util');

const Choices = require('inquirer');
const InquirerAutocomplete = require('inquirer-autocomplete-prompt');
const stripAnsi = require('strip-ansi');
const style = require('ansi-styles');
const fuzzy = require('fuzzy');

const readdir = util.promisify(fs.readdir);




function getPaths(rootPath, pattern, excludePath, itemType) {
  const fuzzOptions = {
    pre: style.green.open,
    post: style.green.close
  };

  async function listNodes(nodePath) {
    // console.log(nodePath); //      ..\..\app\docs
    // const projectPath = path.resolve(__dirname, "../../");
    //
    // console.log(projectPath);

    try {
      if (excludePath(nodePath)) {
        return [];
      }
      const nodes = await readdir(nodePath);


      const currentNode = (itemType !== 'file' ? [nodePath] : []);

      // console.log("currentNode", currentNode);

      if (nodes.length > 0) {

        nodes.map(nodeName => {
            // console.log("nodePath", nodePath);
            return true;
          }
        );

        const nodesWithPath = nodes.map(
          nodeName => listNodes(path.join(nodePath, nodeName))
        );
        const subNodes = await Promise.all(nodesWithPath);
        return subNodes.reduce((acc, val) => acc.concat(val), currentNode);
      }
      return currentNode;
    } catch (err) {
      if (err.code === 'ENOTDIR') {
        return itemType !== 'directory' ? [nodePath] : [];
      }
      return [];
    }
  }

  const nodes = listNodes(rootPath);
  const filterPromise = nodes.then(
    nodeList => fuzzy
      .filter(pattern || '', nodeList, fuzzOptions)
      .map(e => e.string)
  );
  return filterPromise;
}

class InquirerFuzzyPath extends InquirerAutocomplete {
  constructor(question, rl, answers) {
    const rootPath = question.rootPath || '.';
    const excludePath = question.excludePath || (() => false);
    const itemType = question.itemType || 'any';
    const questionBase = Object.assign(
      {},
      question,
      { source: (_, pattern) => getPaths(rootPath, pattern, excludePath, itemType) }
    );
    super(questionBase, rl, answers);
  }

  search(searchTerm) {
    return super.search(searchTerm)
      .then(() => {
        this.currentChoices.getChoice = (choiceIndex) => {
          const choice = Choices.prototype.getChoice.call(this.currentChoices, choiceIndex);
          return {
            value: stripAnsi(choice.value),
            name: stripAnsi(choice.name),
            short: stripAnsi(choice.name)
          };
        };
      });
  }

  onSubmit(line) {
    super.onSubmit(stripAnsi(line));
  }

}


module.exports = InquirerFuzzyPath;
